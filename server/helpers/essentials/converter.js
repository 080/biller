// Export object
var convertionHelper = {};

// Function to convert a customerIdentifiers's value pair into key:value
convertionHelper.convertValuesToKeyValue = (data) => {
	return data.map((attribute) => {
		let temp = {};
		temp[attribute.attributeName] = attribute.attributeValue;
		return temp;
	});
};

// Export functions
module.exports = convertionHelper;
