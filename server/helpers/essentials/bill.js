// Export object
var billHelper = {};

// Function to check if the request matches the bill
billHelper.checkBillAuthenticity = (bill, body) => {
	let responseObject = {
		status: false,
		reason: "default",
	};

	// Process bills differently based on conditions
	switch (bill.amountExactness) {
		case "EXACT":
			if (
				bill.aggregates.total.amount.value ===
				body.paymentDetails.billAmount.value
			) {
				responseObject.status = true;
			} else {
				responseObject.reason = "exact-case-amount-not-matched";
			}
			break;
		default:
			responseObject.reason = "no-case-matched";
	}

	return responseObject;
};

// Export functions
module.exports = billHelper;
