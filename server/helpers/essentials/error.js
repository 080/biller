// Export object
var errorHelper = {};

// Required helpers
const responseHelper = require("./response");

// Function to report error
errorHelper.errorInCatch = (res, error) => {
	// Custom error object
	let errorMessage = {};

	errorMessage.status = 500;
	errorMessage.message =
		error.errmsg || error.message || "Something went wrong.";

	// Respond with error
	console.log("Error", errorMessage);

	responseHelper.generateAndSendResponse(res, false, "unknown-error");
};

// Export functions
module.exports = errorHelper;
