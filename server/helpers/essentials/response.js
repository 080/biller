// Required libraries
const _ = require("lodash");

// Required constants
const errorConstants = require("../../constants/error");

// Export object
var responseHelper = {};

// Function to generate success response for fetchCustomerBills
responseHelper.generateFetchSuccessResponse = (customer) => {
	let responseObject = {};
	let bills = customer.bills;
	let billStatus = "NO_OUTSTANDING";

	if (bills.length > 0) billStatus = "AVAILABLE";

	_.set(responseObject, "customer.name", customer.name);
	_.set(responseObject, "billDetails.billFetchStatus", billStatus);
	_.set(responseObject, "billDetails.bills", bills);

	return responseObject;
};

// Function to generate and send responses
responseHelper.generateAndSendResponse = (res, success, data) => {
	let responseObject = {};

	// below code is not optimized intentionally to maintain response ordering
	if (success) {
		responseObject.status = 200;
		responseObject.success = success;
		responseObject.data = data;
	} else {
		let errorData = errorConstants[data];
		responseObject.status = errorData.statusCode || 500;
		responseObject.success = success;
		responseObject.error =
			errorData.statusDetails || "Something went wrong";
	}

	return res.status(responseObject.status).send(responseObject);
};

// Export functions
module.exports = responseHelper;
