// Require environment
const env = require("../../../environment");

// Required libraries
const jwt = require("jsonwebtoken");

// Required helpers
const responseHelper = require("../../helpers/essentials/response");

// Export object
var jwtMiddleware = {};

// Function to generate responses
jwtMiddleware.checkAuthentication = (req, res, next) => {
	try {
		// Check if token exists
		if (typeof req.headers.authorization !== "undefined") {
			// Gather token from header
			let token = req.headers.authorization.split(" ")[1];

			// Verify the token
			jwt.verify(token, env.JWT_SECRET, (err, decoded) => {
				if (err) {
					// Send appropriate error details
					responseHelper.generateAndSendResponse(
						res,
						false,
						err.name
					);
				} else {
					// Check if schema id matches
					if (decoded.aud === env.SCHEMA_ID) {
						next();
					} else {
						// Send error if schema id does not match
						responseHelper.generateAndSendResponse(
							res,
							false,
							"schema-id-mismatch"
						);
					}
				}
			});
		} else {
			// send appropriate error if authentication header is missing
			responseHelper.generateAndSendResponse(
				res,
				false,
				"auth-header-missing"
			);
		}
	} catch (error) {
		console.log("Error in jwtMiddleware.checkAuthentication - ", error);
		errorHelper.errorInCatch(res, error);
	}
};

// Export functions
module.exports = jwtMiddleware;
