// Required constants
const schemaConstants = require("../../constants/schema");

// Required helpers
const responseHelper = require("../../helpers/essentials/response");

// Export object
var schemaMiddleware = {};

// Function to check if the request schema is valid
schemaMiddleware.checkSchema = (schemaType) => {
	let schema = schemaConstants[schemaType];

	return (req, res, next) => {
		schema
			.isValid(req.body)
			.then((valid) => {
				if (valid) next();
				else {
					responseHelper.generateAndSendResponse(
						res,
						false,
						"request-schema-invalid"
					);
				}
			})
			.catch((error) => {
				console.log("Error in schemaMiddleware.checkSchema - ", error);
				errorHelper.errorInCatch(res, error);
			});
	};
};

// Export functions
module.exports = schemaMiddleware;
