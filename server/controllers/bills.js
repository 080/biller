// Reqiured libraries
const _ = require("lodash");

// Required models
const BillModel = require("mongoose").model("Bill");
const ReceiptModel = require("mongoose").model("Receipt");
const CustomerModel = require("mongoose").model("Customer");

// Required helpers
const billHelper = require("../helpers/essentials/bill");
const errorHelper = require("../helpers/essentials/error");
const responseHelper = require("../helpers/essentials/response");
const convertionHelper = require("../helpers/essentials/converter");

// Export object
var billsController = {};

// Fetch Customer Bills API endpoint
billsController.fetchCustomerBills = async (req, res) => {
	try {
		let identifiers = req.body.customerIdentifiers;
		identifiers = convertionHelper.convertValuesToKeyValue(identifiers);

		// Find and populate customer details
		let customer = await CustomerModel.findOne({
			$or: identifiers,
		}).populate([
			{
				path: "bills",
				match: { status: { $nin: ["PAID", "OTHER"] } },
				populate: {
					path: "customerAccount",
					select: "id",
				},
			},
		]);

		if (customer) {
			// return customer details
			let body = responseHelper.generateFetchSuccessResponse(
				customer.toObject()
			);
			responseHelper.generateAndSendResponse(res, true, body);
		} else {
			// return that customer is not found
			responseHelper.generateAndSendResponse(
				res,
				false,
				"customer-not-found"
			);
		}
	} catch (error) {
		console.log("Error in billsController.fetchCustomerBills - ", error);
		errorHelper.errorInCatch(res, error);
	}
};

// Fetch Bill Receipt API endpoint
billsController.fetchBillReceipt = async (req, res) => {
	try {
		let body = req.body;

		// Find the respective bill
		let bill = await BillModel.findOne({
			billerBillID: body.billerBillID,
		});

		// Check if bill exists
		if (bill) {
			// Check if status is DUE or anything else
			if (bill.status !== "DUE") {
				// If the status is not due raise bill-already-fulfilled error
				responseHelper.generateAndSendResponse(
					res,
					false,
					"bill-already-fulfilled"
				);
			} else {
				// Check authenticity of bill with request
				let authenticityObject = billHelper.checkBillAuthenticity(
					bill,
					body
				);

				// Process the bill if authenticity passes
				if (authenticityObject.status) {
					// Create the receipt object
					let receiptObject = {};
					let paymentDetailsObject = body.paymentDetails;

					// Add the recript details
					_.set(receiptObject, "billerBillID", body.billerBillID);
					_.set(receiptObject, "platformBillID", body.platformBillID);

					_.set(
						receiptObject,
						"platformTransactionRefID",
						paymentDetailsObject.platformTransactionRefID
					);
					_.set(
						receiptObject,
						"uniquePaymentRefID",
						paymentDetailsObject.uniquePaymentRefID
					);
					_.set(
						receiptObject,
						"customerAccount",
						bill.customerAccount
					);

					let savedReceipt = new ReceiptModel(receiptObject);
					let customer = CustomerModel.findById(bill.customerAccount);

					// Check if customer exists
					if (customer) {
						// Create the receipt and update necessary details
						let responseAll = await Promise.all([
							savedReceipt.save(),
							bill.update({
								status: "PAID",
							}),
							customer.update({
								$push: {
									receipts: savedReceipt,
								},
							}),
						]);

						// send response of receipt
						responseHelper.generateAndSendResponse(
							res,
							true,
							responseAll[0].toObject()
						);
					} else {
						// send appropriate error if customer is not found
						responseHelper.generateAndSendResponse(
							res,
							false,
							"customer-not-found"
						);
					}
				} else {
					// send appropraite error if authenticity check fails
					responseHelper.generateAndSendResponse(
						res,
						false,
						authenticityObject.reason
					);
				}
			}
		} else {
			// send appropriate error if the requested bill does not exist
			responseHelper.generateAndSendResponse(
				res,
				false,
				"bill-not-found"
			);
		}
	} catch (error) {
		console.log("Error in billsController.fetchBillReceipt - ", error);
		errorHelper.errorInCatch(res, error);
	}
};

// Export functions
module.exports = billsController;
