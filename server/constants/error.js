module.exports = {
	"customer-not-found": {
		statusCode: 404,
		statusDetails: {
			code: "customer-not-found",
			title: "Customer not found",
			detail:
				"The requested customer was not found in the biller system.",
			traceID: "",
			docURL: "",
		},
	},
	"bill-not-found": {
		statusCode: 404,
		statusDetails: {
			code: "bill-not-found",
			title: "Bill not found",
			detail: "The requested bill was not found in the biller system.",
			traceID: "",
			docURL: "",
		},
	},
	"bill-already-fulfilled": {
		statusCode: 409,
		statusDetails: {
			code: "bill-already-fulfilled",
			title: "Bill already fulfilled",
			detail:
				"The requested bill was not due, it was either paid or is getting processed.",
			traceID: "",
			docURL: "",
		},
	},
	"exact-case-amount-not-matched": {
		statusCode: 409,
		statusDetails: {
			code: "exact-case-amount-not-matched",
			title: "Exact case amount not matched",
			detail:
				"The amount in our bill and the one you sent are different.",
			traceID: "",
			docURL: "",
		},
	},
	"no-case-matched": {
		statusCode: 409,
		statusDetails: {
			code: "no-case-matched",
			title: "No case matched",
			detail: "None of the amountExactness cases matched in our system.",
			traceID: "",
			docURL: "",
		},
	},
	"auth-header-missing": {
		statusCode: 401,
		statusDetails: {
			code: "auth-header-missing",
			title: "Auth header missing",
			detail: "Authentication header was not present in the request.",
			traceID: "",
			docURL: "",
		},
	},
	"auth-header-wrong": {
		statusCode: 401,
		statusDetails: {
			code: "auth-header-wrong",
			title: "Auth header wrong",
			detail: "Authentication header provided is incorrect.",
			traceID: "",
			docURL: "",
		},
	},
	"request-schema-invalid": {
		statusCode: 400,
		statusDetails: {
			code: "request-schema-invalid",
			title: "Request schema invalid",
			detail: "The request you sent failed schema validation.",
			traceID: "",
			docURL: "",
		},
	},
	"schema-id-mismatch": {
		statusCode: 406,
		statusDetails: {
			code: "schema-id-mismatch",
			title: "Schema id mismatch",
			detail: "Schema Id provided in authentication header is incorrect.",
			traceID: "",
			docURL: "",
		},
	},
	"unknown-error": {
		statusCode: 500,
		statusDetails: {
			code: "unknown-error",
			title: "Unknown error",
			detail:
				"There seems so be some kind of error processing your request, we will check and revert shortly.",
			traceID: "",
			docURL: "",
		},
	},
	TokenExpiredError: {
		statusCode: 401,
		statusDetails: {
			code: "auth-token-expired",
			title: "Auth token expired",
			detail: "Authentication header provided has expired.",
			traceID: "",
			docURL: "",
		},
	},
	JsonWebTokenError: {
		statusCode: 401,
		statusDetails: {
			code: "auth-token-error",
			title: "Auth token error",
			detail: "Authentication header provided couldn't be processed.",
			traceID: "",
			docURL: "",
		},
	},
	NotBeforeError: {
		statusCode: 401,
		statusDetails: {
			code: "auth-token-not-before-error",
			title: "Auth token not before error",
			detail: "Authentication header provided is not yet active.",
			traceID: "",
			docURL: "",
		},
	},
};
