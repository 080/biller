// Required libraries
const yup = require("yup");

const customerIdentifiersSchema = yup.object().shape({
	attributeName: yup.string().required(),
	attributeValue: yup.string().required(),
});

const amountSchema = yup.object().shape({
	value: yup.number().required(),
});

module.exports = {
	fetchCustomerBillsSchema: yup.object().shape({
		customerIdentifiers: yup
			.array()
			.of(customerIdentifiersSchema)
			.required(),
	}),
	fetchBillReceiptSchema: yup.object().shape({
		billerBillID: yup.string().required(),
		platformBillID: yup.string().required(),
		paymentDetails: yup.object().shape({
			platformTransactionRefID: yup.string().required(),
			uniquePaymentRefID: yup.string().required(),
			amountPaid: amountSchema,
			billAmount: amountSchema,
		}),
	}),
};
