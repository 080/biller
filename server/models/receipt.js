const { nanoid } = require("nanoid");
const moment = require("moment");
const mongoose = require("mongoose");
const mongooseHidden = require("mongoose-hidden")();

const ReceiptSchema = mongoose.Schema({
	billerBillID: {
		type: String,
		index: true,
		required: true,
		unique: true,
	},
	platformBillID: {
		type: String,
		index: true,
		required: true,
	},
	platformTransactionRefID: {
		type: String,
		index: true,
		required: true,
	},
	uniquePaymentRefID: {
		type: String,
		index: true,
		required: true,
	},
	receipt: {
		id: {
			type: String,
			required: true,
			unique: true,
			default: () => nanoid(),
		},
		date: {
			type: String,
			required: true,
			default: () => moment().utc().format(),
		},
	},
	customerAccount: {
		type: mongoose.Schema.Types.ObjectId,
		ref: "Customer",
		hide: true,
	},
});

ReceiptSchema.plugin(mongooseHidden);
module.exports = mongoose.model("Receipt", ReceiptSchema);
