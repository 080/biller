const mongoose = require("mongoose");
const mongooseHidden = require("mongoose-hidden")();

const CustomerSchema = mongoose.Schema({
	name: {
		type: String,
		index: true,
		required: true,
	},
	mobileNumber: {
		type: String,
		index: true,
		required: true,
		unique: true,
	},
	id: {
		type: String,
		require: true,
		default: () => Math.floor(Math.random() * 10000) + 1,
	},
	bills: [
		{
			type: mongoose.Schema.Types.ObjectId,
			ref: "Bill",
		},
	],
	receipts: [
		{
			type: mongoose.Schema.Types.ObjectId,
			ref: "Receipt",
		},
	],
});

CustomerSchema.plugin(mongooseHidden);
module.exports = mongoose.model("Customer", CustomerSchema);
