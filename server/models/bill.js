const mongoose = require("mongoose");
const moment = require("moment");
const mongooseHidden = require("mongoose-hidden")();

const BillSchema = mongoose.Schema({
	billerBillID: {
		type: String,
		index: true,
		required: true,
		unique: true,
	},
	status: {
		type: String,
		enum: ["DUE", "PAID", "OTHER"],
		default: "DUE",
		required: true,
		hide: true,
	},
	generatedOn: {
		type: String,
		required: true,
		default: () => moment().utc().format(),
	},
	recurrence: {
		type: String,
		enum: [
			"ONE_TIME",
			"WEEKLY",
			"DAILY",
			"MONTHLY",
			"FORTNIGHTY",
			"YEARLY",
			"HALF_YEARLY",
			"QUARTERLY",
			"AS_PRESENTED",
		],
		default: "ONE_TIME",
		required: true,
	},
	amountExactness: {
		type: String,
		enum: ["EXACT", "EXACT_UP", "EXACT_DOWN", "RANGE", "ANY"],
		default: "EXACT",
		required: true,
	},
	customerAccount: {
		type: mongoose.Schema.Types.ObjectId,
		ref: "Customer",
	},
	aggregates: {
		total: {
			displayName: {
				type: String,
				enum: ["Total Outstanding"],
				default: "Total Outstanding",
				required: true,
			},
			amount: {
				value: {
					type: Number,
					required: true,
					index: true,
				},
			},
		},
	},
});

BillSchema.plugin(mongooseHidden);
module.exports = mongoose.model("Bill", BillSchema);
