const router = require("express").Router();

router.use("/bills", require("./api/bills"));

module.exports = router;
