const router = require("express").Router();

// Controllers
const billsController = require("../../controllers/bills");

// Middlewares
const jwtMiddleware = require("../../helpers/middlewares/jwt");
const schemaMiddleware = require("../../helpers/middlewares/schema");

// Fetch Bills API
router.post(
	"/fetch",
	jwtMiddleware.checkAuthentication,
	schemaMiddleware.checkSchema("fetchCustomerBillsSchema"),
	billsController.fetchCustomerBills
);

// Fetch Receipts API
router.post(
	"/fetchReceipt",
	jwtMiddleware.checkAuthentication,
	schemaMiddleware.checkSchema("fetchBillReceiptSchema"),
	billsController.fetchBillReceipt
);

module.exports = router;
