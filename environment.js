require("dotenv").config();

module.exports = {
	BACKEND_PORT: process.env.PORT,
	MONGO_CONNECTION_URI: process.env.MONGO_CONNECTION_URI,
	MONGO_DEBUG: process.env.MONGO_DEBUG,
	SCHEMA_ID: process.env.SCHEMA_ID,
	JWT_SECRET: process.env.JWT_SECRET,
};
