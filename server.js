// Require environment
const env = require("./environment");

// Require packages
const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require("cors");
const helmet = require("helmet");
const morgan = require("morgan");

// Start server
const app = express();

// Basic express config
app.use(morgan("dev"));
app.use(helmet());
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Connect to database with mongoose
mongoose.connect(env.MONGO_CONNECTION_URI, {
	useNewUrlParser: true,
	useFindAndModify: false,
	useCreateIndex: true,
	useUnifiedTopology: true,
});
mongoose.set("debug", env.MONGO_DEBUG);

// Models
require("./server/models/customer");
require("./server/models/bill");
require("./server/models/receipt");

// Routes
app.use(require("./server/routes"));

// Catch 404 and forward to error handler
app.use(function (req, res, next) {
	var err = new Error("Not Found");
	err.status = 404;
	next(err);
});

// Development error middleware - Print StackTrace
app.use((err, req, res, next) => {
	console.log("Server Error Middleware ", err.stack);
	res.status(err.status || 500);
	res.send({ message: "Something went wrong" });
});

// Server start point
app.listen(env.BACKEND_PORT, (err) => {
	if (err) return new Error("Server not connected");
	console.log(`Listening to port ${env.BACKEND_PORT}`);
});
