# Biller

Code that implements Biller part of the Setu's APIs

# Features

-   Passes all tests implemented in the [Postman collection](https://docs.setu.co/biller-quickstart/postman)
-   JWT based authentication
-   Yup based validation
-   Scripts to generate and test APIs locally

# Assumptions

1. A customer can have multiple `customerIdentifiers` sent in request
2. A customer can have multiple outstanding bills
3. A customer's `id` is something different than the `_id` and is common across all bills shared by that user
4. `platformBillID`, `platformTransactionRefID`, `uniquePaymentRefID` are thought to be random data sent from Setu
5. Only one bill can be paid at a time
6. If a paid bill is submitted then it'll return `bill-already-fulfilled`
7. Some mismatches were there in the [Error codes](https://docs.setu.co/error-codes) so i resorted to using generic ones

# Usage

`npm run start` - starts the server

`npm run customer` - adds a customer into the database, edit `scripts/createcustomer.js` to personalize it

`npm run bills` - adds and tags bills for a customer into the database, edit `scripts/addbills.js` to personalize it

`npm run token` - generates the JWT token valid for 2 minutes

# Structure

```
├── scripts
│   ├── addbills.js - Used to add bills for a customer
│   ├── createcustomer.js - Used to create a customer
│   └── generateToken.js - Used to generate a JWT token
├── server
│   ├── constants - Holds error and schema information
│   ├── controllers - Main execution logic for APIs
│   ├── helpers
│   │   ├── essentials - Holds essential helper functions
│   │   └── middlewares - Holds middleware functions
│   ├── models - Holds the database schemas
│   └── routes - Access point to all routes
├── environment.js - Used in managing the .env file
├── server.js - Main entry point
├── package-lock.json
├── package.json
├── README.md
└── .gitignore
```

# Error codes

`customer-not-found` - The requested customer was not found in the biller system.

`bill-not-found` - The requested bill was not found in the biller system.

`bill-already-fulfilled` - The requested bill was not due, it was either paid or is getting processed.

`exact-case-amount-not-matched` - The amount in our bill and the one you sent are different.

`no-case-matched` - None of the `amountExactness` cases matched in our system.

`auth-header-missing` - Authentication header was not present in the request.

`auth-header-wrong` - Authentication header provided is incorrect.

`request-schema-invalid` - The request you sent failed schema validation.

`schema-id-mismatch` - Schema Id provided in authentication header is incorrect.

`auth-token-expired` - Authentication header provided has expired.

`auth-token-error` - Authentication header provided couldn't be processed.

`auth-token-not-before-error` - Authentication header provided is not yet active.

`unknown-error` - An unexpected error which does not match any above error criteria.
