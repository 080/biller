// Require environment
const env = require("../environment");

// Require packages
const _ = require("lodash");
const mongoose = require("mongoose");

// Connect to database with mongoose
mongoose.connect(env.MONGO_CONNECTION_URI, {
	useNewUrlParser: true,
	useFindAndModify: false,
	useCreateIndex: true,
	useUnifiedTopology: true,
});
mongoose.set("debug", env.MONGO_DEBUG);

// Load models
require("../server/models/customer");
require("../server/models/bill");

// Required models
const BillModel = require("mongoose").model("Bill");
const CustomerModel = require("mongoose").model("Customer");

const addBills = async () => {
	try {
		// Create variable to store values
		let billDetails = {};

		let customer = await CustomerModel.findOne({
			mobileNumber: "8971302465",
		});

		if (customer) {
			for (let i = 0; i < 5; i++) {
				// Generate bill details
				billDetails.billerBillID = Math.random()
					.toString(36)
					.substring(7);
				billDetails.customerAccount = customer;
				_.set(
					billDetails,
					"aggregates.total.amount.value",
					Math.floor(Math.random() * 1000) + 1
				);

				let savedBill = new BillModel(billDetails);
				if (savedBill) {
					await Promise.all([
						savedBill.save(),
						customer.update({
							$push: {
								bills: savedBill,
							},
						}),
					]);
					console.log("Successfully added Bill");
				}
			}
		} else {
			console.log("No such customer exists");
		}
	} catch (error) {
		console.log("Something went wrong in adding Bill");
		console.error(error);
	}
};

addBills();
