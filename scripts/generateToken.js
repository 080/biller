const env = require("../environment");
const jwt = require("jsonwebtoken");
const { nanoid } = require("nanoid");

var payload = {
	aud: env.SCHEMA_ID,
	iat: Math.round(Date.now() / 1000),
	jti: nanoid(32),
};

const generateAccessToken = (payload) => {
	console.log(
		jwt.sign(payload, env.JWT_SECRET, {
			expiresIn: "2m",
			notBefore: "0s",
		})
	);
};

generateAccessToken(payload);
