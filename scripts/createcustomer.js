// Require environment
const env = require("../environment");

// Require packages
const mongoose = require("mongoose");

// Connect to database with mongoose
mongoose.connect(env.MONGO_CONNECTION_URI, {
	useNewUrlParser: true,
	useFindAndModify: false,
	useCreateIndex: true,
	useUnifiedTopology: true,
});
mongoose.set("debug", env.MONGO_DEBUG);

// Load models
require("../server/models/customer");

// Required models
const CustomerModel = require("mongoose").model("Customer");

const createCustomer = async () => {
	try {
		// Create variable to store values
		let customerDetails = {};

		customerDetails.name = "Nithin Kashyap";
		customerDetails.mobileNumber = 9871302465;

		let savedCustomer = new CustomerModel(customerDetails);

		await savedCustomer.save();

		console.log("Successfully created Customer");
	} catch (error) {
		console.log("Something went wrong in creating Customer");
	}
};

createCustomer();
